package vn.urbox.api;

import com.typesafe.config.Config;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.junit.Before;
import org.junit.Test;
import vn.urbox.config.Configs;
import vn.urbox.exception.ApiFailureException;
import vn.urbox.okhttp.HttpMethod;

import java.io.IOException;

public class SampleApiTest {
    protected ApiClient apiClient;
    protected Config config;

    @Before
    public void before() {
        apiClient = new ApiClient();
        config = Configs.newBuilder().withResource("api/api-url.conf").build();

    }

    @Test
    public void sampleTestCase() throws IOException, ApiFailureException {
        String url = config.getString("devapi.card-createIssue");
        RequestBody body = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("app_id", "29")
                .addFormDataPart("app_secret", "4bce06890a12312309ce0qfeqwe")
                .addFormDataPart("create_id", "33")
                .addFormDataPart("money", "150000000")
                .addFormDataPart("quantity", "2")
                .addFormDataPart("gift_id", "1249")
                .addFormDataPart("needActive", "2")
                .addFormDataPart("description", "Sample desc")
                .addFormDataPart("hasPin", "1")
                .build();
        apiClient.setRequest(new Request.Builder().url(url).post(body).build());

        int responseCode = apiClient.httpExecute();
        System.out.println(responseCode);
        System.out.println(apiClient.getResponseJson());

        responseCode = apiClient.executeRequest(HttpMethod.POST, url, null, body);
        System.out.println(responseCode);
        System.out.println(apiClient.getResponseJson());

    }
}
