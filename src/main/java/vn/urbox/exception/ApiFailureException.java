package vn.urbox.exception;

public class ApiFailureException extends Exception {
    public ApiFailureException(String message) {
        super(message);
    }

    public ApiFailureException(Exception e) {
        super(e);
    }

    public ApiFailureException(String message, Exception e) {
        super(message, e);
    }
}