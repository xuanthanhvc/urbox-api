package vn.urbox.okhttp;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

public class HttpManager {
    private static final ThreadLocal<OkHttpClient> tlClient = new ThreadLocal<>();

    private HttpManager() {
    }

    public static OkHttpClient getTlClient() {
        if (tlClient.get() == null) {
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
            final OkHttpClient client = new OkHttpClient.Builder()
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .readTimeout(30, TimeUnit.SECONDS)
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();
            tlClient.set(client);
        }
        return tlClient.get();
    }

    public static void shutdown() {
        if (tlClient.get() != null) {
            tlClient.get().dispatcher().executorService().shutdown();
        }
        tlClient.remove();
    }
}
