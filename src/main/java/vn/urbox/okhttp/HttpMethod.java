package vn.urbox.okhttp;

public enum HttpMethod {
    GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE, CONNECT, MOVE, PROXY, PRI;
}
