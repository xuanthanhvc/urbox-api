package vn.urbox.api;

import lombok.Getter;
import lombok.Setter;
import okhttp3.*;
import okio.Buffer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import vn.urbox.exception.ApiFailureException;
import vn.urbox.okhttp.HttpManager;
import vn.urbox.okhttp.HttpMethod;

import java.io.IOException;

@Getter
@Setter
public class ApiClient {
    protected OkHttpClient httpClient;
    protected String url;
    protected Request request;
    protected Response response;
    protected ResponseBody responseBody;
    protected JSONObject responseJson;
    protected String responseString;
    private final Logger log = LogManager.getLogger(this.getClass());

    public ApiClient() {
        this.httpClient = HttpManager.getTlClient();
    }

    public ApiClient(String url) {
        this.url = url;
        this.httpClient = HttpManager.getTlClient();
    }

    private static String bodyToString(final RequestBody requestBody) {
        try {
            final Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }


    public int httpExecute() throws IOException {
        String methodString = request.method();
        log.debug("Executing {} request to {}", methodString, request.url());
        response = httpClient.newCall(request).execute();
        int responseCode = response.code();
        log.debug("Response code: {}", responseCode);
        responseBody = response.body();
        if (responseBody != null) {
            responseString = responseBody.string();
            try {
                responseJson = responseString.isEmpty() ? null : new JSONObject(responseString);
                log.debug("Response body: \n{}", responseJson);
            } catch (JSONException e) {
                log.debug("Response String: {}", responseString);
            }
        }
        return responseCode;
    }

    public int executeRequest(HttpMethod method) throws IOException, ApiFailureException {
        log.debug("Executing request {}", method);
        if (method == HttpMethod.GET) {
            request = new Request.Builder().url(url).get().build();
        } else if (method == HttpMethod.DELETE) {
            request = new Request.Builder().url(url).delete().build();
        } else {
            throw new UnsupportedOperationException("Unsupported request HttpMethod");
        }
        int responseCode = httpExecute();
        if (responseCode >= 400) {
            throw new ApiFailureException(responseString);
        }
        return responseCode;
    }

    public int executeRequest(HttpMethod method, RequestBody body) throws IOException, ApiFailureException {
        String bodyString = bodyToString(body);
        log.debug("Executing request {} with body\n{}", method, bodyString);
        if (method == HttpMethod.POST) {
            request = new Request.Builder().url(url).post(body).build();
        } else if (method == HttpMethod.PUT) {
            request = new Request.Builder().url(url).put(body).build();
        } else if (method == HttpMethod.PATCH) {
            request = new Request.Builder().url(url).patch(body).build();
        } else {
            throw new UnsupportedOperationException("Unsupported request HttpMethod");
        }
        int responseCode = httpExecute();
        if (responseCode >= 400) {
            throw new ApiFailureException(responseString);
        }
        return responseCode;
    }

    public int executeRequest(HttpMethod method, String url, Headers headers, RequestBody body) throws IOException, ApiFailureException {
        String bodyString = bodyToString(body);
        log.debug("Executing request {} with body\n{}", method, bodyString);
        switch (method) {
            case POST:
                request = new Request.Builder().url(url).post(body).build();
                break;
            case PUT:
                request = new Request.Builder().url(url).put(body).build();
                break;
            case PATCH:
                request = new Request.Builder().url(url).patch(body).build();
                break;
            default:
                throw new UnsupportedOperationException("Unsupported request HttpMethod");
        }
        int responseCode = httpExecute();
        if (responseCode >= 400) {
            throw new ApiFailureException(responseString);
        }
        return responseCode;
    }
}